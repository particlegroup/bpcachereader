/*
 * cacheframe.h
 *
 *  Created on: 04 Jul 2017
 *      Author: dylan
 */

#ifndef CACHEFRAME_H_
#define CACHEFRAME_H_

#include <vector>
struct Particle
{
	unsigned int index;
	float x[3];
	float v[3];
	/*float r[4];
	float av[3];
	float size;
	float t[3];*/
};

struct Header
{
	char title[8];
	unsigned int type;
	unsigned int totpoints;
	unsigned int dtypes;
};

struct CacheFrame
{
	Header header;
	std::vector<Particle> particles;
};



#endif /* CACHEFRAME_H_ */
